
//@ts-ignore
let localization = function(inputCity){
    const appid = '3b30486246de0e3709d118ddd0f12390'
    //@ts-ignore
    const url = `https://api.openweathermap.org/data/2.5/weather?q=${inputCity}&units=metric&lang=fr&appid=${appid}`
    
    fetch(url)
        .then((resp) => resp.json())
        .then(function (data) {
            //@ts-ignore
            let windKmH = parseInt(data.wind.speed * 3.6);
                document.getElementById("selectCityName").innerHTML = data.name
                document.getElementById("selectCityTempMax").innerHTML = data.main.temp_max
                document.getElementById("selectCityTempMin").innerHTML = data.main.temp_min
                document.getElementById("selectCityHumidity").innerHTML = data.main.humidity
                //@ts-ignore
                document.getElementById("selectCityWindSpeed").innerHTML = windKmH
    
                if(data.wind.deg <= 30 && data.wind.deg > 330){
                    document.getElementById("selectCityWindDirection").innerHTML = `<span class="material-icons">
                    east
                    </span>Est`
                }
                else if(data.wind.deg <=60  && data.wind.deg > 30){
                    document.getElementById("selectCityWindDirection").innerHTML = `<span class="material-icons">
                    north_east
                    </span>Nord-Est`
                }
                else if(data.wind.deg <= 120 && data.wind.deg > 60){
                    document.getElementById("selectCityWindDirection").innerHTML = `<span class="material-icons">
                    north
                    </span>Nord`
                }
                else if(data.wind.deg <= 150 && data.wind.deg > 120){
                    document.getElementById("selectCityWindDirection").innerHTML = `<span class="material-icons">
                    north_west
                    </span>Nord-Oust`
                }
                else if(data.wind.deg <= 210 && data.wind.deg > 150){
                    document.getElementById("selectCityWindDirection").innerHTML = `<span class="material-icons">
                    west
                    </span>Oust`
                }
                else if(data.wind.deg <= 240 && data.wind.deg > 210){
                    document.getElementById("selectCityWindDirection").innerHTML = `<span class="material-icons">
                    south_west
                    </span>Sud-Oust`
                }
                else if(data.wind.deg <= 300 && data.wind.deg > 240){
                    document.getElementById("selectCityWindDirection").innerHTML = `<span class="material-icons">
                    south
                    </span>Sud`
                }
                else if(data.wind.deg <= 330 && data.wind.deg > 300){
                    document.getElementById("selectCityWindDirection").innerHTML = `<span class="material-icons">
                    south_east
                    </span>Sud-Est`
                }
    
                document.getElementById("selectCityWeatherIcon").innerHTML = `<img src =http://openweathermap.org/img/wn/${data.weather[0].icon}@4x.png>`
            })
        }
    
    localization("Paris");
    let submit = document.getElementById("submit");
    submit.addEventListener("click", getCityName);
    
    function getCityName() {
        //@ts-ignore
        const inputCity = document.getElementById("inputCity").value
        localization(inputCity)
    }
    